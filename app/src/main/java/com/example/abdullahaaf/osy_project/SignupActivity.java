package com.example.abdullahaaf.osy_project;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by abdullahaaf on 18/02/17.
 */

public class SignupActivity extends Activity {

    private static int result_load_image = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Button btnloadimage = (Button) findViewById(R.id.button_upload_gambar);
        btnloadimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(
                    Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, result_load_image);
            }
        });
    }

    public void onActivityResult(int requestcode, int resultcode, Intent data){
        super.onActivityResult(requestcode,resultcode,data);

        if (requestcode == result_load_image && resultcode == RESULT_OK && null != data){
            Uri selectedimage = data.getData();
            String[] filepathcolumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedimage,filepathcolumn,null,null,null);
            cursor.moveToFirst();

            int columindex = cursor.getColumnIndex(filepathcolumn[0]);
            String picturepath = cursor.getString(columindex);
            cursor.close();

            ImageView imageView = (ImageView) findViewById(R.id.gambarprofil);
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturepath));
        }
    }
}
